#!/usr/bin/env bash

source change_variable
brew install parallel --quiet
make -C ..
mv ../push_swap .
if [ "$TEST_CHECKER" == "yes" ]; then
    make bonus -C ..
    mv ../checker .
fi
parallel --citation <<EOF
will cite
EOF
./srcs/test_push_swap.sh
