#!/usr/bin/env bash
#set -euo pipefail

source change_variable

OS="$(uname -s)"

if [ "$OS" = "Linux" ]; then
	checker_os()
	{
		./srcs/checker_linux "$@"
	}
elif [ "$OS" = "Darwin" ]; then
	checker_os()
	{
		./srcs/checker_Mac "$@"
	}
else
	echo "Can't find OS"
fi

export -f checker_os


test_pushswap()
{
	MIN="$1"
	MAX="$2"
	MAX_INSTR="$3"
	ARGS="$(ruby -e "puts ($MIN..$MAX).to_a.shuffle.join(' ')")";
	PUSH_S="$(./push_swap $ARGS)"
	if [ -z "$PUSH_S" ]; then
		CHECK_OS="$(printf "" | checker_os $ARGS)"
		if [ "$TEST_CHECKER" == "yes" ]; then
			CHECK="$(printf "" | ./checker $ARGS)"
		fi
	else
		CHECK_OS="$(echo "$PUSH_S" | checker_os $ARGS)"
		if [ "$TEST_CHECKER" == "yes" ]; then
			CHECK="$(echo "$PUSH_S" | ./checker $ARGS)"
		fi
	fi
	if [ "$CHECK_OS" == "KO" ]; then
		echo "Error C_M : KO" >&2
	fi
	if [ "$TEST_CHECKER" == "yes" ] && [ "$CHECK" == "KO" ]; then
		echo "Error c : KO" >&2
	fi
	INSTR="$(echo -n "$PUSH_S" | wc -l)"
	if [ "$INSTR" -gt "$MAX_INSTR" ]; then
		echo "Error :"  "$INSTR"
		echo "Error :"  "$INSTR" >&2
		echo "$ARGS" >&2
	fi
}

test_good_return ()
{
	ARGS1="$1"
	TEST="$2"
	PUSH_S="$(./push_swap $ARGS1 2>&1)"
	echo -e "\033[36m TEST $TEST: \033[0m"
	if [[ "$PUSH_S" != "Error" ]]; then
		echo "ERROR : BAD RETURN"
	else
		echo -e "\033[32m SUCCESS \033[0m"
	fi
	echo
}

test_good_return_int ()
{
	ARGS1="$1"
	TEST="$2"
	PUSH_S="$(./push_swap $ARGS1 2>&- | checker_os $ARGS1 )"
	echo -e "\033[36m TEST $TEST: \033[0m"
	if [[ "$PUSH_S" != "OK" ]]; then
		echo "ERROR : BAD RETURN"
	else
		echo -e "\033[32m SUCCESS \033[0m"
	fi
	echo
}

export -f test_pushswap

launcher_test()
{
	RES1=""
	RES2=""
	RES3=""
	MIN1="$1"
	MAX1="$2"
	MIN2="$3"
	MAX2="$4"
	MIN3="$5"
	MAX3="$6"
	INSTR_MAX="$7"
	SEQ1="$8"
	SEQ2="$9"
	SEQ3="${10}"
	TEST="${11}"
	echo -e "\033[36m TEST $TEST: \033[0m"
	echo -e "Echelle : [\033[33m$1\033[0m .. \033[33m$2\033[0m] | Nb de test : $SEQ1"
	{ RES1="$(seq $SEQ1 | parallel --bar --eta test_pushswap $MIN1 $MAX1 $INSTR_MAX 2>&3)"; } 3>&2
	echo -e "Echelle : [\033[33m$3\033[0m .. \033[33m$4\033[0m] | Nb de test : $SEQ2"
	{ RES2="$(seq $SEQ2 | parallel --bar --eta test_pushswap $MIN2 $MAX2 $INSTR_MAX 2>&3)"; } 3>&2
	echo -e "Echelle : [\033[33m$5\033[0m .. \033[33m$6\033[0m] | Nb de test : $SEQ3"
	{ RES3="$(seq $SEQ3 | parallel --bar --eta test_pushswap $MIN3 $MAX3 $INSTR_MAX 2>&3)"; } 3>&2
	if [ -z "$RES1" ] && [ -z "$RES2" ] && [ -z "$RES3" ]; then
		echo -e "\033[32m SUCCESS \033[0m"
	else
		DIVIDEND="$(echo "$RES1" "$RES2" "$RES3" | wc -l)"
		echo "ERROR : $(($DIVIDEND * 100 / ($SEQ1 + $SEQ2 + $SEQ3)))%"
	fi
	echo
}

export -f test_good_return
export -f test_good_return_int

test_good_return "1 3 2 4 3" "DUP NUMBERS"
test_good_return "ab cd efg" "AREN'T INTEGERS"
test_good_return "-2147483649 3 -35" "LESS THAN INT MIN"
test_good_return "2147483648 3 -35" "MORE THAN INT MAX"
test_good_return_int "-2147483648 4 7 3" "INT MIN"
test_good_return_int "2147483647 4 7 3" "INT MAX"
launcher_test -2 0 1 3 -3 -1 3 10 10 10 3
launcher_test -5 -1 -2 2 1 5 12 30 30 40 5
launcher_test -100 0 -50 49 1 100 "${MAX_INSTR_100}" 30 30 40 100
launcher_test -500 0 -250 249 1 500 "${MAX_INSTR_500}" 30 30 40 500
